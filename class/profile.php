<?php

namespace pongsit\profile;

class profile extends \pongsit\model\model{
	
	public function __construct(){
		parent::__construct();
	}

	function show_more($inputs){
		// $exam_id='';
		// if(!empty($inputs['exam_id'])){ $exam_id=$inputs['exam_id']; }
		$order_by='id';
		if(!empty($inputs['order_by'])){ $order_by=$inputs['order_by']; }
		$sort='DESC';
		if(!empty($inputs['sort'])){ $sort=$inputs['sort']; }
		$limit=-1;
		if(!empty($inputs['limit'])){ $limit=$inputs['limit']; }
		$offset=0;
		if(!empty($inputs['offset'])){ $offset=$inputs['offset']; }
		// $query = "SELECT * FROM ".$this->table." WHERE exam_id='$exam_id' ORDER BY $order_by $sort LIMIT $limit OFFSET $offset;";
		$query = "SELECT * FROM ".$this->table." ORDER BY $order_by $sort LIMIT $limit OFFSET $offset;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	
	// this one is backward compatible
	function get_info($id){
		$query = "SELECT * FROM ".$this->table." WHERE user_id = '".$id."';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs)){
			return $outputs;
		}
	}
	
	function get_feature_image($id){
		$path_to_core=$GLOBALS['path_to_core'];
		$path_to_root=$GLOBALS['path_to_root'];
		$variables['photo']=$path_to_core.'img/profile/male-avatar.png';
		if(file_exists($path_to_root.'custom/img/profile/'.$id.'.jpg')){
			$variables['photo']=$path_to_root.'custom/img/profile/'.$id.'.jpg';
		}else if(file_exists($path_to_root.'custom/img/profile/'.$id.'.png')){
			$variables['photo']=$path_to_root.'custom/img/profile/'.$id.'.png';
		}
		return $variables['photo'];
	}
	
	function get_all_infos($id){
		$outputs = $this->get_infos($id);
		$outputs['feature_image'] = $this->get_feature_image($id);
		return $outputs;
	}
	
}