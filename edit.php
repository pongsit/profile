<?php
	
require_once("../system/init.php");

$profile = new \pongsit\profile\profile();
$option = new \pongsit\option\option();
$role = new \pongsit\role\role();

if(empty($_GET['filter_id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create($variables);
	exit();
}

$filter_id =+$_GET['filter_id'];

if(!($role->check('admin') || $role->check('manager') || $role->check('staff') || $filter_id==$_SESSION['user']['id'])){
	$view = new \pongsit\view\view('message');
	echo $view->create(array('message'=>'คุณไม่มีสิทธิ์ใช้หน้านี้ครับ'));
	exit();
}

$user_profile = '';
$profile_alls = $profile->get_all_column_active(array('order_by'=>'weight','sort'=>'asc'));

// print_r($profile_alls);
// Array ( [0] => Array ( [id] => 3 [name] => title [name_show] => ยศ [weight] => 40 [multiple] => 0 [json] => [active] => 1 ) 
if(!empty($profile_alls)){
	$plus_dones=array();
	$minus_dones=array();
	// loop through all active columns
	foreach($profile_alls as $values){
		// Array ( [id] => 1 [name] => first_name [name_show] => ชื่อ [weight] => [multiple] => 0 [json] => [active] => 1 ) 
		// print_r($values);
		// exit();
		$number='';
		if(!empty($values['json'])){
			$json_obj = json_decode($values['json']);
			if($json_obj->type=='number'){
				$number = 'number';
			}
		}
		$variables['label']=$values['name_show'];
		if($values['multiple']==1){
			$add_on='';
			$profile_values = $profile->get_value_filter($values['id'],$filter_id);
			if(!empty($profile_values)){
				foreach($profile_values as $_values){
					if(!in_array($values['id'],$plus_dones)){
						$variables['label']=$values['name_show'].' <a class="plus-column" style="position:relative; top:3px;" href="javascript:;"><i class="far fa-plus-square"></i></a>';
						$plus_dones[]=$values['id'];
					}else{
						if(!in_array($values['id'],$minus_dones)){
							$variables['label']='';
							$add_on='<span class="input-group-addon ml-2"><a class="minus-column" style="position:relative; top:3px;" href="javascript:;"><i class="far fa-minus-square"></i></a></span>';
							$minus_dones[]=$values['id'];
						}
					}
					$variables['info']='<div class="input-group multiple-input-wrapper">
										  	<input data-column_id="'.$values['id'].'" data-value_id="'.$_values['id'].'" name="profile_values['.$_values['id'].']" class="form-control multiple-first text-right '.$number.'" type="text" value="'.$_values['value'].'"> 
											'.$add_on.'
										</div>';
					$user_profile .= $view->block('info-list-row',$variables);
				}
			}else{
				$variables['label'].=' <a class="plus-column" href="javascript:;"><i class="far fa-plus-square"></i></a>';
				$variables['info']='<div class="input-group multiple-input-wrapper">
										<input data-column_id="'.$values['id'].'" name="profile_multiples['.$values['id'].'][]" class="form-control text-right '.$number.'" type="text" value="">
									</div>';
				$user_profile .= $view->block('info-list-row',$variables);
			}
		}else{
			$profile_values = $profile->get_column_value($values['id'],$filter_id);
			$profile_value='';
			// print_r($profile_values);
			// Array ( [id] => 1 [profile_column_id] => 1 [filter_id] => 1 [value] => พงศิษฏ์ )
			if(!empty($profile_values['id'])){
				if(!empty($profile_values['value'])){ $profile_value=$profile_values['value'];}
				$variables['info']='<input name="profile_values['.$profile_values['id'].']" class="form-control text-right '.$number.'" type="text" value="'.$profile_value.'">';
				$user_profile .= $view->block('info-list-row',$variables);
			}else{
				$variables['info']='<input name="profiles['.$values['id'].']" class="form-control text-right '.$number.'" type="text" value="">';
				$user_profile .= $view->block('info-list-row',$variables);
			}
		}
	}
}

$variables=array();
$variables['h1'] = $view->block('h1',array('message'=>'แบบฟอร์มแก้ข้อมูล','css'=>'col-md-7 text-center'));
if($_POST){
	// print_r($_POST);
	// exit();
	// Array ( [profile_values] => Array ( [1] => พงศิษฏ์ ) [profiles] => Array ( [2] => ทวิชพงศ์ธร [3] => เอส ) ) 
	// Array ( [profile_values] => Array ( [4] => สมชาย [5] => กล้าหาญ [6] => ชาย ) ) 

	if(!empty($_POST['profile_multiple_deletes'])){
		foreach($_POST['profile_multiple_deletes'] as $_value_id){
			$profile->delete_value($_value_id);
		}
		unset($_POST['profile_multiple_deletes']);
	}
	if(!empty($_POST['profile_multiples'])){
		foreach($_POST['profile_multiples'] as $_column_id=>$values){
			foreach($values as $_key=>$_value){
				$profile->insert_value(array('profile_column_id'=>$_column_id, 'value'=>$_value, 'filter_id'=>$filter_id));
			}
		}
		unset($_POST['profile_multiple']);
	}
	if(!empty($_POST['profile_values'])){
		foreach($_POST['profile_values'] as $_id=>$_value){
			$profile->update_value(array('id'=>$_id,'value'=>$_value));
		}
		unset($_POST['profile_values']);
	}
	if(!empty($_POST['profiles'])){
		foreach($_POST['profiles'] as $_column_id=>$_value){
			$profile->insert_value_once(array('profile_column_id'=>$_column_id, 'value'=>$_value, 'filter_id'=>$filter_id));
		}
		unset($_POST['profiles']);
	}
	header('Location:'.$path_to_core.'user/info.php?id='.$filter_id.'&notification=profile-edited');
}
$variables['profile-list']='';
if(!empty($user_profile)){
	$variables['profile-list']=$user_profile;
}else{
	if($role->check('admin')){
		$variables['profile-list']='<div class="w-100 text-center"><a href="'.$path_to_core.'profile/column-list.php">เพิ่มแบบฟอร์มข้อมูลผู้ใช้ที่ต้องการ</a></div>';
	}
}
echo $view->create($variables);

