<?php
		
require_once("../system/init.php");
$profile= new \pongsit\profile\profile();

if(empty($_POST['type'])){
	exit();
}

switch($_POST['type']){
	case 'quick-search-id': 
		$results = $profile->quick_search(@$_POST['q']);
		break;
	case 'show-more': 
		// $_check = '<img style="width:24px;" src="'.$path_to_core.'system/img/icon/check.png">';
		$_check = '<i class="fas fa-check-square"></i>';
		// $_uncheck = '<img style="width:24px;" src="'.$path_to_core.'system/img/icon/check-box-empty.png">';
		$_uncheck = '<i class="far fa-square"></i>';
		$results = $profile->get_all_column($_POST); 
		foreach($results as $key=>$values){
			foreach($values as $_key=>$_value){
				switch($_key){
					case 'active': $results[$key]['active'] = ($_value?$_check:$_uncheck); break;
					case 'multiple': $results[$key]['multiple'] = ($_value?$_check:$_uncheck); break;
					case 'json': $results[$key]['json'] = (!empty($_value)?$_check:$_uncheck); break;
				}
			}
		}
		break;
}

if(empty($results)){
	exit();
}

header('Content-Type: application/json');
echo json_encode($results);