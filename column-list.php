<?php

require_once("../system/init.php");

$profile = new \pongsit\profile\profile();

$variables['list'] = $view->ajax('show-more',array(
'url'=>'ajax.php',
'block'=>'column-list',
'json'=>'{"limit":"20","offset":"0","order_by":"weight","sort":"asc"}'
));

if(!empty($_GET['notification'])){
	switch($_GET['notification']){
		case 'profile-edited': $variables['notification'] = $view->block('alert',array('message'=>'แก้ไขเรียบร้อย','type'=>'success','css'=>'col-12'));
	}
}
$variables['page-name'] = 'แบบฟอร์มข้อมูลผู้ใช้';
// $variables['h1']=$view->block('h1',array('message'=>'ข้อมูลผู้ใช้','css'=>'col-12 text-center'));
echo $view->create($variables);