<?php

require_once("../system/init.php");
$profile = new \pongsit\profile\profile();
$role = new \pongsit\role\role();

if(!($role->check('admin') || $role->check('staff') || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('message');
	echo $view->create(array('message'=>'คุณไม่สามารถใช้งานหน้านี้ได้ครับ'));
	exit();
}

if(empty($_GET['id'])){
	$view = new \pongsit\view\view('message');
	echo $view->create(array('message'=>'มีบางอย่างผิดพลาด'));
	exit();
}
$id = +$_GET['id'];

$profile_infos = $profile->get_column_info($id);

if(!empty($_POST['name'])){
	$_POST['id']=$profile_infos['id'];
	if($profile->update_column($_POST)){
		header('Location:'.$path_to_core.'profile/column-list.php?notification=profile-edited');
		exit();
	}else{
		$variables['notification'] = $view->block('alert',array('type'=>'danger','message'=>'มีบางอย่างผิดพลาดครับ','css'=>'col-md-8'));
	}
}
$variables['name'] = @$profile_infos['name'];
$variables['page-name'] = 'แก้ช่อง Profile';
// $variables['h1']=$view->block('h1',array('message'=>'แก้ไขแบบฟอร์ม Profile','css'=>'col-md-8 text-center'));
$variables['acive_option_1']='';
$variables['acive_option_0']='';
if($profile_infos['active']==1){
	$variables['acive_option_1']='selected';
}else{
	$variables['acive_option_0']='selected';
}
$variables['active_css']='';
$variables['json']= htmlentities(@$profile_infos['json']);
$variables['name_show']= @$profile_infos['name_show'];
$variables['weight']= @$profile_infos['weight'];
$variables['multiple_option_1']='';
$variables['multiple_option_0']='';
if($profile_infos['multiple']==1){
	$variables['multiple_option_1']='selected';
}else{
	$variables['multiple_option_0']='selected';
}
$view = new \pongsit\view\view('column-insert');
echo $view->create($variables);