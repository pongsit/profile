CREATE TABLE `profile_column` (
  `id` INT AUTO_INCREMENT primary key NOT NULL,
  `name` text,
  `name_show` text,
  `weight` int(11) NOT NULL DEFAULT '50',
  `multiple` int(11) DEFAULT '0',
  `json` text,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profile_value` (
  `id` INT AUTO_INCREMENT primary key NOT NULL,
  `profile_column_id` int(11) DEFAULT NULL,
  `filter_id` int(11) DEFAULT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;