<?php

require_once("../system/init.php");

$profile = new \pongsit\profile\profile();
$role = new \pongsit\role\role();

if(!($role->check('admin') || $role->check('staff') || $_SESSION['user']['id'] == 1)){
	$view = new \pongsit\view\view('message');
	echo $view->create(array('message'=>'คุณไม่สามารถใช้งานหน้านี้ได้ครับ'));
	exit();
}

if(!empty($_POST['name'])){
	if(empty($profile->get_column_id($_POST['name']))){
		if($profile->insert_column($_POST)){
			$variables['notification'] = $view->block('alert',array('type'=>'success','message'=>'เพิ่มข้อมูลเรียบร้อย','css'=>'col-md-8'));
			header('Location: '.$path_to_core.'profile/column-list.php');
			exit();
		}
	}else{
		$variables['notification'] = $view->block('alert',array('type'=>'danger','message'=>'มีข้อมูลในฐานข้อมูลแล้วครับ','css'=>'col-md-8'));
	}
}
$variables['name'] = '';
$variables['description'] = '';
$variables['page-name'] = 'เพิ่มช่อง Profile';
$variables['acive_option_1']='';
$variables['acive_option_0']='';
$variables['active_css']='hide';
$variables['json']= '';
$variables['name_show']= '';
$variables['weight']= '';
$variables['multiple_option_1']='';
$variables['multiple_option_0']='selected';
echo $view->create($variables);